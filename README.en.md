# seckill-dubbo

#### Description
在imooc上克隆名称为wtiscm人的代码。他的介绍如下：
跟着老师吧整个项目做了一边，收获很多；然后把老师视频中提到的MQ消息队列内容加进来了。最后不断整合以及重构，构建了一套分布式秒杀系统，SpringBoot+Dubbo+Zookeeper，利用页面静态化，CDN缓存，Nignx缓存，以及服务端的页面缓存，Redis对象缓存，层层过滤，防止访问直接穿透到数据库；采用异步消息队列分担短时间的数据库读写；利用Dubbo分布式RPC调用降低耦合与容错，保证整个项目可用；更多细节可以看源码，一起讨论，一起学习进步，项目源码：https://github.com/wtiscm/seckill-dubbo

#### Software Architecture
Software architecture description

#### Installation

1. xxxx
2. xxxx
3. xxxx

#### Instructions

1. xxxx
2. xxxx
3. xxxx

#### Contribution

1. Fork the repository
2. Create Feat_xxx branch
3. Commit your code
4. Create Pull Request


#### Gitee Feature

1. You can use Readme\_XXX.md to support different languages, such as Readme\_en.md, Readme\_zh.md
2. Gitee blog [blog.gitee.com](https://blog.gitee.com)
3. Explore open source project [https://gitee.com/explore](https://gitee.com/explore)
4. The most valuable open source project [GVP](https://gitee.com/gvp)
5. The manual of Gitee [https://gitee.com/help](https://gitee.com/help)
6. The most popular members  [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)